<?php
/* Template name: advanced_custom_fields_contacto */

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<div>
    <?= get_header() ?>
</div>

<section class="container">
    <div class="row justify-content-md-center m-5"  style="
        background: rgb(38, 34, 47);
        border-radius: 16px;
        box-shadow: rgba(63, 55, 87,0.5) 5px 6px 7px;">
        <div class="col-auto text-light p-5">
            <h3 class="text-light">Contacto</h3>
            <?= the_content() ?>
        </div>
    </div>
</section>

<?= get_footer() ?>
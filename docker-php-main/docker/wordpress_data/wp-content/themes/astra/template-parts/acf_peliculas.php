<?php
/* Template name: advanced_custom_fields_peliculas */

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<div>
    <?= get_header() ?>
</div>

<p>
    <!-- HERO -->
    <?
    $image = get_field('hero');
    if (!empty($image)) : ?>

<section style="background-image:url('<?php echo esc_url($image['url']); ?>');
    min-height:690px;
    height: 690px;
    margin:0;
    background-position: center left;
    background-repeat: no-repeat;
    background-size: auto;
    max-width: 100%;
    box-shadow:inset 0 0 0 2000px rgba(0, 0, 0, 0.66);
    margin-top: -32px;">

<?php endif; ?>

<div class="row justify-content-md-center" style="height:100%;">
    <div class="col-6 ">
        <h1 class="text-light" style="margin-top: 50%;font-size:9em;">
            <span style="
                color:red;
                display: inline-block;
                width: 0.4em;">
            <? 
            echo substr(get_field('hero_title_peliculas'),0,1);
            ?>
            </span>
            <? 
            echo substr(get_field('hero_title_peliculas'),1);
            ?>
        </h1>
    </div>
</div>
</section>
<!-- seccion pelicula 1 -->
<section class="container mt-5 mb-5">
    <!-- pelicula 1 -->
    <div class="row justify-content-md-center p-3 my-5" style="
        background: rgb(38, 34, 47);
        border-radius: 16px;
        box-shadow: rgba(63, 55, 87,0.5) 5px 6px 7px;">
        <!-- imagen peli -->
        <div class="col-4">

            <?
            $pel1 = get_field('pel_home1');
            if (!empty($pel1)) : ?>
                <img src="<?php echo esc_url($pel1['url']); ?>" style="max-width:500px;max-height:384px;">
            <?php endif; ?>
            
        </div>
        <!-- info peli -->
        <div class="col-8">
        <h3 style="color: white;"><? the_field('pel1_home_title'); ?></h3><br>
        <h4 style="color: white;">Sinopsis:</h4>
        <p style="color: white;"><? the_field('pel1_desc'); ?></p>
        <h4 style="color: white; display: inline;">Dirección: </h4>
        <span style="color: white;"><? the_field('pel1_dir'); ?></span>
        <h4 style="color: white;">Reparto:</h4>
        <p style="color: white;"><? the_field('pel1_rep'); ?></p>
        </div>
    </div>
    <!-- pelicula 2 -->
    <div class="row justify-content-md-center p-3 my-5" style="
        background: rgb(38, 34, 47);
        border-radius: 16px;
        box-shadow: rgba(63, 55, 87,0.5) 5px 6px 7px;">
        <!-- imagen peli -->
        <div class="col-4">

            <?
            $pel2 = get_field('pel_home2');
            if (!empty($pel2)) : ?>
                <img src="<?php echo esc_url($pel2['url']); ?>" style="max-width:500px;max-height:384px;">
            <?php endif; ?>
            
        </div>
        <!-- info peli -->
        <div class="col-8">
        <h3 style="color: white;"><? the_field('pel2_home_title'); ?></h3><br>
        <h4 style="color: white;">Sinopsis:</h4>
        <p style="color: white;"><? the_field('pel2_desc'); ?></p>
        <h4 style="color: white; display: inline;">Dirección: </h4>
        <span style="color: white;"><? the_field('pel2_dir'); ?></span>
        <h4 style="color: white;">Reparto:</h4>
        <p style="color: white;"><? the_field('pel2_rep'); ?></p>
        </div>
    </div>
    <!-- pelicula 3 -->
    <div class="row justify-content-md-center p-3 my-5" style="
        background: rgb(38, 34, 47);
        border-radius: 16px;
        box-shadow: rgba(63, 55, 87,0.5) 5px 6px 7px;">
        <!-- imagen peli -->
        <div class="col-4">

            <?
            $pel3 = get_field('pel_home3');
            if (!empty($pel3)) : ?>
                <img src="<?php echo esc_url($pel3['url']); ?>" style="max-width:500px;max-height:384px;">
            <?php endif; ?>
            
        </div>
        <!-- info peli -->
        <div class="col-8">
        <h3 style="color: white;"><? the_field('pel3_home_title'); ?></h3><br>
        <h4 style="color: white;">Sinopsis:</h4>
        <p style="color: white;"><? the_field('pel3_desc'); ?></p>
        <h4 style="color: white; display: inline;">Dirección: </h4>
        <span style="color: white;"><? the_field('pel3_dir'); ?></span>
        <h4 style="color: white;">Reparto:</h4>
        <p style="color: white;"><? the_field('pel3_rep'); ?></p>
        </div>
    </div>
</section>


<?= get_footer() ?>
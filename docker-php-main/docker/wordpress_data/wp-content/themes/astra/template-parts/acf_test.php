<?php
/* Template name: advanced_custom_fields_test */

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<div>
    <?= get_header() ?>
</div>

<p>
    <!-- HERO -->
    <?
    $image = get_field('hero');
    if (!empty($image)) : ?>

<section style="background-image:url('<?php echo esc_url($image['url']); ?>');
    min-height:690px;
    height: 690px;
    margin:0;
    background-position: center left;
    background-repeat: no-repeat;
    background-size: auto;
    max-width: 100%;
    box-shadow:inset 0 0 0 2000px rgba(0, 0, 0, 0.66);
    margin-top: -32px;">

<?php endif; ?>

<div class="row justify-content-md-center" style="height:100%;">
    <div class="col-6 ">
        <h1 class="text-light" style="margin-top: 50%;font-size:9em;">
            <span style="
                color:red;
                display: inline-block;
                width: 0.4em;">
            <? 
            echo substr(get_field('hero_title'),0,1);
            ?>
            </span>
            <? 
            echo substr(get_field('hero_title'),1);
            ?>
        </h1>
    </div>
</div>
</section>
<!-- seccion peliculas -->
<section class="container mt-5">
    <div class="row">
        <div class="col-12">
            <h3 style="font-size:3.5em;"><? the_field('peliculas_home'); ?></h3>
        </div>
    </div>
    <!-- lista peliculas -->
    <div class="row justify-content-md-center p-3" style="
        background: rgb(38, 34, 47);
        border-radius: 16px;
        box-shadow: rgba(63, 55, 87,0.5) 5px 6px 7px;">
        <div class="col-4 text-center">

            <?
            $pel1 = get_field('pel_home1');
            if (!empty($pel1)) : ?>
                <img src="<?php echo esc_url($pel1['url']); ?>" style="max-width:300px;max-height:300px;">
            <?php endif; ?>
            <h4 style="color: white;"><? the_field('pel1_home_title'); ?></h4>
        </div>

        <div class="col-4  text-center">

            <?
            $pel1 = get_field('pel_home2');
            if (!empty($pel1)) : ?>
                <img src="<?php echo esc_url($pel1['url']); ?>" style="max-width:300px;max-height:300px;">
            <?php endif; ?>
            <h4 style="color: white;"><? the_field('pel2_home_title'); ?></h4>
        </div>

        <div class="col-4  text-center">

            <?
            $pel1 = get_field('pel_home3');
            if (!empty($pel1)) : ?>
                <img src="<?php echo esc_url($pel1['url']); ?>" style="max-width:300px;max-height:300px;">
            <?php endif; ?>
            <h4 style="color: white;"><? the_field('pel3_home_title'); ?></h4>
        </div>
    </div>
</section>

<!-- seccion series -->
<section class="container mt-5 mb-5">
    <div class="row">
        <div class="col-12">
            <h3 style="font-size:3.5em;"><? the_field('series_home'); ?></h3>
        </div>
    </div>
    <!-- lista sericulas -->
    <div class="row justify-content-md-center p-3" style="
        background: rgb(38, 34, 47);
        border-radius: 16px;
        box-shadow: rgba(63, 55, 87,0.5) 5px 6px 7px;">
        <div class="col-4 text-center">

            <?
            $ser1 = get_field('ser_home1');
            if (!empty($ser1)) : ?>
                <img src="<?php echo esc_url($ser1['url']); ?>" style="max-width:300px;max-height:300px;">
            <?php endif; ?>
            <h4 style="color: white;"><? the_field('ser1_home_title'); ?></h4>
        </div>

        <div class="col-4  text-center">

            <?
            $ser1 = get_field('ser_home2');
            if (!empty($ser1)) : ?>
                <img src="<?php echo esc_url($ser1['url']); ?>" style="max-width:300px;max-height:300px;">
            <?php endif; ?>
            <h4 style="color: white;"><? the_field('ser2_home_title'); ?></h4>
        </div>

        <div class="col-4  text-center">

            <?
            $ser1 = get_field('ser_home3');
            if (!empty($ser1)) : ?>
                <img src="<?php echo esc_url($ser1['url']); ?>" style="max-width:300px;max-height:300px;">
            <?php endif; ?>
            <h4 style="color: white;"><? the_field('ser3_home_title'); ?></h4>
        </div>
    </div>
</section>

<?= get_footer() ?>